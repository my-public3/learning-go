package main

import "fmt"

func main() {
	var price float64 = 100
	fmt.Println("price is ", price, "dollar")

	var taxrate float64 = 0.08
	var tax float64 = price * taxrate
	fmt.Println("tax is ", tax, "dollar")

	var total float64 = price + tax
	fmt.Println("tota; price is ", total, "dollar")

	var availableFunds float64 = 120
	fmt.Println("available funds", availableFunds)
	fmt.Println("within budget?", total <= availableFunds)

}
